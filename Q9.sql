SELECT category.category_name , SUM(item.item_price) AS total_price
FROM item
INNER JOIN item_category category
ON item.category_id = category.category_id
GROUP BY item.category_id
ORDER BY total_price DESC;
